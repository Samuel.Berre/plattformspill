package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

public class HelloWorld extends InputAdapter implements ApplicationListener {
    private SpriteBatch batch;
    private BitmapFont font;
    
    private TiledMap board;
    private TiledMapTileLayer boardMap;
    private TiledMapTileLayer player;
    private TiledMapTileLayer hole;
    private TiledMapTileLayer flag;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    
    private Cell playerNormal;
    private Cell playerDead;
    private Cell playerWon;
    private int x;
    private int y;

    @Override
    public void create() {
//        batch = new SpriteBatch();
//        font = new BitmapFont();
//        font.setColor(Color.RED);
    	
    	Gdx.input.setInputProcessor(this);
    	
    	TmxMapLoader map = new TmxMapLoader();
    	board = map.load("gameboard.tmx");
    	boardMap = (TiledMapTileLayer) board.getLayers().get("Board");
    	player = (TiledMapTileLayer) board.getLayers().get("Player");
    	hole = (TiledMapTileLayer) board.getLayers().get("Hole");
    	flag = (TiledMapTileLayer) board.getLayers().get("Flag");
    	
    	camera = new OrthographicCamera();
    	camera.setToOrtho(false, 5, 5);
    	camera.position.x = 2.5f;
    	camera.update();
    	
    	float render = (1f/300f);
    	renderer = new OrthogonalTiledMapRenderer(board, render);
    	renderer.setView(camera);
    	renderer.render();
    	
    	Texture player = new Texture("player.png");
    	TextureRegion[][] trs = TextureRegion.split(player, 300, 300);
    	
    	playerNormal = new Cell().setTile(new StaticTiledMapTile(trs[0][0]));
    	playerDead = new Cell().setTile(new StaticTiledMapTile(trs[0][1]));
    	playerWon = new Cell().setTile(new StaticTiledMapTile(trs[0][2]));

    	
    }
    @Override
    public boolean keyUp(int keycode){
    	if(keycode == Input.Keys.LEFT) {
    		player.setCell(x, y, null);
    		player.setCell(x-1, y, playerNormal);
    		x -= 1;
    	}
    	if(keycode == Input.Keys.RIGHT) {
    		player.setCell(x+1, y, playerNormal);
    		player.setCell(x, y, null);
    		x += 1;
    		
    	}
    	if(keycode == Input.Keys.UP) {
    		player.setCell(x, y, null);
    		player.setCell(x, y+1, playerNormal);
    		y += 1;
    		
    	}
    	if(keycode == Input.Keys.DOWN) {
    		player.setCell(x, y, null);
    		player.setCell(x, y-1, playerNormal);
    		y -= 1;
    		
    	}
		return false;
    	
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        
        renderer.render();
        player.setCell(x, y, playerNormal);
        
        if(hole.getCell(x, y) != null) {
    		this.player.setCell(x, y, playerDead);
    	}
    	if(flag.getCell(x, y) != null) {
    		this.player.setCell(x, y, playerWon);
    	}

//        batch.begin();
//        font.draw(batch, "Hello World", 200, 200);
//        batch.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
